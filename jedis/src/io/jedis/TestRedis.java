package io.jedis;

import java.util.Set;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class TestRedis {

	public void test() {
		JedisPool pool = new JedisPool(new JedisPoolConfig(), "localhost");

		Jedis jedis = null;
		try {
			// jedis = new Jedis("localhost");
			jedis = pool.getResource();
			jedis.auth("unkonwn");
			String value = jedis.get("foo");
			System.out.println(value);
			value = jedis.get("123");
			System.out.println(value);
			value = jedis.get("a");
			System.out.println(value);
			jedis.zadd("sose", 0, "car");
			jedis.zadd("sose", 0, "bike");
			Set<String> sose = jedis.zrange("sose", 0, -1);
			for (String string : sose) {
				System.out.println(string);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (jedis != null)
				jedis.close();
		}
		// / ... when closing your application:
		pool.destroy();
	}
}
